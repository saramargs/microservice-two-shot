from django.contrib import admin
from .models import Hat

# Register your models here.


@admin.register(Hat)
class HatAdmim(admin.ModelAdmin):
    list_display = [
        "fabric",
        "style",
        "color",
    ]

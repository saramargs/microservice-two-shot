from .models import Hat, LocationVO
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["href", "name"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "pictureURL",
        "location",
    ]
    encoders = {"location": LocationVOEncoder()}


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style", "id"]


@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            HatDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': "invalid Location ID"},
                status=400, )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def detail_hats(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        content, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": content > 0})


@require_http_methods(["GET"])
def list_locationvo(request):
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationVOEncoder,
            safe=False
        )

from django.urls import path
from .views import list_hats, list_locationvo, detail_hats


urlpatterns = [
    path("hats/", list_hats, name="hats_list"),
    # path("hats/<int:id>/", detail_hats, name="hats_details")
    path("locations/", list_locationvo, name="location_list"),
    path("hats/<int:id>/", detail_hats, name="hat_details"),
]

import React, { useEffect, useState } from 'react'

function ShoeForm (props) {

    
    const [bins, setBins] = useState([])
    
    const [manufacturer, setManufacturer] = useState('')
    const handleManChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const [model, setModel] = useState('')
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const [color, setColor] = useState('')
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    
    const [picture, setPic] = useState('')
    const handlePicChange = (event) => {
        const value = event.target.value
        setPic(value)
    }
    
    const [bin, setBin] = useState('')
    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/bins/'
        
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            console.log(data.bins)
            setBins(data.bins)
        }
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.manufacturer = manufacturer
        data.model_name = model
        data.color = color
        data.picture_url = picture
        data.bin = bin

        console.log(data)

        const shoesUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)

            setManufacturer('')
            setModel('')
            setColor('')
            setPic('')
            setBin('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleManChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"></input>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelChange}  value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control"></input>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange}  value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicChange}  value={picture} placeholder="Picture Url" required type="text" name="picture" id="picture" className="form-control"></input>
                <label htmlFor="picture">Picture Url</label>
              </div>
              <div className="mb-3">
              <select onChange={handleBinChange} value={bin} required id="closet"  name="closet" className="form-select">
                <option value="">Choose a closet</option>
                {bins.map(bin => {
                    return (
                    <option key={bin.import_href} value={bin.href}>
                        {bin.closet_name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ShoeForm
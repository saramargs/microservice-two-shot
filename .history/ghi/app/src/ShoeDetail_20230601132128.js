import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';


function ShoeDetail() {
    
    let { id } = useParams();

  const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8080/api/shoes/${id}`
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setShoes(data.shoe);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
    return (
    <>
    <div className="card mb-3 shadow">
        <img src={shoe.picture_url} className="card-img-top"></img>
        <div className="card-body">
            
        </div>
    </div>
    </>);
  }

  export default ShoeDetail;
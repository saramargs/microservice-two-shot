import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';


function ShoeDetail() {
    
    let { id } = useParams();

  const [shoe, setShoe] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8080/api/shoes/${id}`
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setShoe(data);
        console.log(data)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
    return (
    <>
    <div className="card mb-3 shadow">
        <img src={shoe.picture_url} className="card-img-top"></img>
        <div className="card-body">
            <h5 className="card-title">
                {shoe.manufacturer}
            </h5>
            <h6 className="card-subtitle mb-2 text-muted">
                {shoe.model_name}
            </h6>
            <p className="card-text">
                Color: {shoe.color
            </p>
        </div>
    </div>
    </>);
  }

  export default ShoeDetail;
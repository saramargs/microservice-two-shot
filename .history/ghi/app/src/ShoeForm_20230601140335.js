import React, { useEffect, useState } from 'react'

function ShoeForm (props) {

    
    const [bins, setBins] = useState([])
    
    const [manufacturer, setManufacturer] = useState('')
    const handleManChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const [model, setModel] = useState('')
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const [color, setColor] = useState('')
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    
    const [picture, setPic] = useState('')
    const handlePicChange = (event) => {
        const value = event.target.value
        setPic(value)
    }
    
    const [bin, setBin] = useState('')
    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/bins/'
        
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            console.log(data.bins)
            setLocations(data.bins)
        }
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.manufacturer = manufacturer
        data.model_name = model
        data.color = color
        data.picture_url = picture
        data.bin = bin

        console.log(data)

        const shoesUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)

            setManufacturer('')
            setModel('')
            setColor('')
            setPic('')
            setBin('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleManChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"></input>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelChange}  value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control"></input>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange}  value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicChange}  value={picture} placeholder="Picture Url" required type="textarea" name="description" id="description" className="form-control"></input>
                <label htmlFor="city">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPChange}  value={max_presentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="city">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAChange}  value={max_attendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="city">Max Attendees</label>
              </div>
              <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required id="location"  name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm
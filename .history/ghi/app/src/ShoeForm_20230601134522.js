import React, { useEffect, useState } from 'react'

function ShoeForm (props) {

    
    const [bins, setBins] = useState([])
    
    const [manufacturer, setManufacturer] = useState('')
    const handleManChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const [model, setModel] = useState('')
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const [color, setColor] = useState('')
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    
    const [picture, setPic] = useState('')
    const handlePicChange = (event) => {
        const value = event.target.value
        setPic(value)
    }
    
    const [bin, setBin] = useState('')
    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/bins/'
        
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            console.log(data.bins)
            setLocations(data.bins)
        }
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.manufacturer = manufacturer
        data.model_name = model
        data.color = color
        data.picture_url = picture
        data.bin = bin

        console.log(data)

        const shoesUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newLocation)

            setName('')
            setStarts('')
            setEnds('')
            setDesc('')
            setMaxP('')
            setMaxA('')
            setLocation('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange}  value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"></input>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange}  value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"></input>
                <label htmlFor="city">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDescChange}  value={description} placeholder="Description" required type="textarea" name="description" id="description" className="form-control"></input>
                <label htmlFor="city">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPChange}  value={max_presentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="city">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAChange}  value={max_attendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="city">Max Attendees</label>
              </div>
              <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required id="location"  name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm
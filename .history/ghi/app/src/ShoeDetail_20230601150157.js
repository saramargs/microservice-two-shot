import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

let closet = "loading..."
let id = "loading.."


async function deleteShoe(sId) {
  const shoeUrl = `http://localhost:8080/api/shoes/${sId}`
  const fetchConfig = {method: "delete"}

  const response = await fetch(shoeUrl, fetchConfig)
  if (response.ok) {
    console.log("item deleted")
}
}

function ShoeDetail() {
    

  const [shoe, setShoe] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8080/api/shoes/${id}`
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setShoe(data);
        closet = data.bin.closet_name
        id = data.id
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
  console.log(shoe)
    
    return (
    <>
    <div className="card mb-3 shadow">
        <img src={shoe.picture_url} className="card-img-top"></img>
        <div className="card-body">
            <h5 className="card-title">
                {shoe.manufacturer}
            </h5>
            <h6 className="card-subtitle mb-2 text-muted">
                {shoe.model_name}
            </h6>
            <p className="card-text">
                Color: {shoe.color}
            </p>
            <p className="card-text">
              Located in: { closet }
            </p>
            <p className="card-text">
                ID: {shoe.id}
            </p>
            <button className="btn shadow btn-danger" onClick={() => deleteShoe({id})}>Delete</button>
        </div>
    </div>
    </>);
  }

  export default ShoeDetail;
import React, { useEffect, useState } from 'react';

function ShoesList() {

  const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const url = 'http://shoes-api:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
    <>
    <table className="table table-hover table-secondary table-striped border border-dark-subtle shadow container-fluid mt-5">
      <thead className="table-group-divider">
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
        </tr>
      </thead>
      <tbody className="border-top border-dark-subtle">
        {shoes.map(shoe => {
          return (
          <tr className="object-fit" key={shoe.href }>
            <td>{ attendee.name }</td>
            <td>{ attendee.conference }</td>
          </tr>
        );
        })}
      </tbody>
    </table>
    </>);
  }

  export default ShoesList;
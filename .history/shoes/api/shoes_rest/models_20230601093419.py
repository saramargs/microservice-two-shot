from django.db import models


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name =models.CharField(max_length=200)
    
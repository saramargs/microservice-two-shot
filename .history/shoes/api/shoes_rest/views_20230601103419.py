from django.http import JsonResponse
import json
from .models import Shoe, BinVO
from django.views.decorators.http  import require_http_methods
from common.json import *


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color"
    ]


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "close_name"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders ={"bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request)
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False
        )
    else:
        content =json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=binVO_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({'message': "Invalid Bin ID"}, status=400)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


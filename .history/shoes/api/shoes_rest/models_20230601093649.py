from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.CharField(max_length=200)
    bin = models.ForeignKey(
        Bin,
        related_name="bin",
        on_delete=models.PROTECT
    )
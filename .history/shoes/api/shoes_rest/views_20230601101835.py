from django.http import JsonResponse
import json
from .models import Shoe, BinVO
from django.views.decorators.http  import require_http_methods
from common.json import *


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color"
    ]


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, binVO_id):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False
        )
    else:
        content =json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=binVO_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JS


import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

import models from shoes_rest
from shoes_rest.models import BinVO

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            url = "http://monolith:8000/api/conferences"
            response = requests.get(url)
            content = json.loads(response.content)
            for conference in content["conferences"]:
                ConferenceVO.objects.update_or_create(import_href=conference["href"],
            defaults={"name": conference["name"]},
        )

            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

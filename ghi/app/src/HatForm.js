import React, { useEffect, useState } from 'react'


function HatForm () {

    const [locations, setLocations] = useState([])

    const [fabric, setFabric] = useState('')
    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }

    const [style, setStyle] = useState('')
    const handleStyleChange = (event) => {
        const value = event.target.value
        setStyle(value)
    }

    const [color, setColor] = useState('')
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const [picture, setPicture] = useState('')
    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)
    }

    const [location, setLocation] = useState('')
    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/locations/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.fabric = fabric
        data.style = style
        data.color = color
        data.pictureURL = picture
        data.location = location

        const hatsURL = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(hatsURL, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()

            setFabric('')
            setStyle('')
            setColor('')
            setPicture('')
            setLocation('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="manufacturer" id="manufacturer" className="form-control"></input>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange}  value={style} placeholder="Style" required type="text" name="style" id="style" className="form-control"></input>
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange}  value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange}  value={picture} placeholder="Picture URL" required type="text" name="pictureURL" id="picture_url" className="form-control"></input>
                <label htmlFor="pictureURL">Picture URL</label>
              </div>
              <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required id="closet"  name="closet" className="form-select">
                <option value="">Choose a closet</option>
                {locations.map(location => {
                    return (
                    <option key={location.href} value={location.href}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default HatForm

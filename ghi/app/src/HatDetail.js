import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';


let loaded = false
let display = <p>"loading"</p>
let locationName = "loading"


function HatDetail() {

  let { id } = useParams();
  const [hat, setHat] = useState([]);

    const fetchData = async () => {
        const url = `http://localhost:8090/api/hats/${ id }`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHat(data);
            loaded = true;
            locationName = data.location.name
        }}

    const deleteHat = async () => {
        const deleteURL = `http://localhost:8090/api/hats/${ id }`;
        const fetchConfig = {
            method: "delete"
            }
        const response = await fetch(deleteURL, fetchConfig);
        if (response.ok){
            console.log("Successfully deleted")
        }}


    useEffect(() => {
        fetchData();
    }, []);

    if (!loaded) {
        display = <p>Loading...</p>
    }
    else {
        display = <>
        <div className="card mb-3 shadow">
            <img src={hat.pictureURL} className="card-img-top"></img>
            <div className="card-body">
                <h5 className="card-title">
                    {hat.style}
                </h5>
                <h6 className="card-subtitle mb-2 text-muted">
                    {hat.fabric}
                </h6>
                <p className="card-text">
                    Color: {hat.color}
                </p>
                <p className="card-text">
                Located in { locationName }
                </p>
                <p className="card-text">
                    ID: {hat.id}
                </p>
            </div>
        </div>
        <div>
        <Link to="/hats" className="btn shadow btn-danger" onClick={deleteHat}>Delete</Link>
        </div>
        </>
    }

    return (
        display
    )
    // else {
    //     if (!loaded) {
    //         return (
    //         <p>Loading...</p> )
    //     } else {
    //     return (
    //             <>
    //             <div className="card mb-3 shadow">
    //                 <img src={hat.pictureURL} className="card-img-top"></img>
    //                 <div className="card-body">
    //                     <h5 className="card-title">
    //                         {hat.style}
    //                     </h5>
    //                     <h6 className="card-subtitle mb-2 text-muted">
    //                         {hat.fabric}
    //                     </h6>
    //                     <p className="card-text">
    //                         Color: {hat.color}
    //                     </p>
    //                     <p className="card-text">
    //                     Located in { locationName }
    //                     </p>
    //                     <p className="card-text">
    //                         ID: {hat.id}
    //                     </p>
    //                 </div>
    //             </div>
    //             <div>
    //                 <button onClick={deleteHat}>DELETE THIS HAT</button>
    //             </div>
    //             </>) }}
  }

  export default HatDetail;

//   return (
//     <>
//     <div className="card mb-3 shadow">
//         <img src={hat.pictureURL} className="card-img-top"></img>
//         <div className="card-body">
//             <h5 className="card-title">
//                 {hat.style}
//             </h5>
//             <h6 className="card-subtitle mb-2 text-muted">
//                 {hat.fabric}
//             </h6>
//             <p className="card-text">
//                 Color: {hat.color}
//             </p>
//             <p className="card-text">
//             Located in { locationName }
//             </p>
//             <p className="card-text">
//                 ID: {hat.id}
//             </p>
//         </div>
//     </div>
//     <div>
//         <button onClick={deleteHat}>DELETE THIS HAT</button>
//     </div>
//     </>)

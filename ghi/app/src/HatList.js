import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatList() {

  const [hats, setHats] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


    return (
    <>
    <Link to="new/" className="mt-4 btn shadow btn-primary">Add a hat</Link>
    <table className="table table-hover table-secondary table-striped border border-dark-subtle shadow container-fluid mt-5">
      <thead className="table-group-divider">
        <tr>
          <th>Style</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Location</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody className="border-top border-dark-subtle">
        {hats.map(hat => {
          return (
              <tr className="object-fit" key={ hat.id }>
                <td ><Link className="link-underline link-underline-opacity-0" to={ `/hats/${hat.id}` }>{ hat.style }</Link></td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location.name }</td>
                <td>{ hat.pictureURL }</td>
              </tr>
        );
        })}
      </tbody>
    </table>
    </>);
  }

  export default HatList;
